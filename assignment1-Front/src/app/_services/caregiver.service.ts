import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Caregiver } from '../_models/caregiver';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Patient } from '../_models/patient';

@Injectable({
  providedIn: 'root'
})
export class CaregiverService {
  baseUrl = environment.apiUrl + 'caregiver/';

  constructor(private http: HttpClient) { }

    getCaregivers(): Observable<Caregiver[]> {
      return this.http.get<Caregiver[]>(this.baseUrl);
    }
    getCaregiver(id): Observable<Caregiver> {
      return this.http.get<Caregiver>(this.baseUrl + id);
    }

    getPatients(id): Observable<Patient[]> {
      return this.http.get<Patient[]>(this.baseUrl + id + '/patients');
    }

    updateCaregiver(caregiver: Caregiver) {
      return this.http.put(this.baseUrl, caregiver);
    }

    deleteCaregiver(caregiver: Caregiver) {
      return this.http.put(this.baseUrl + '/delete', caregiver);
    }

    addPatientToCaregiver(patientId: number, caregiverId: number) {
      return this.http.put(environment.apiUrl + '/patient/' + patientId + '/addCaregiver/' + caregiverId, null);
    }

}
