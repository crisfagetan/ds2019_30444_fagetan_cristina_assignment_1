import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Patient } from '../_models/patient';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  baseUrl = environment.apiUrl + 'patient/';

  constructor(private http: HttpClient) { }

    getPatients(): Observable<Patient[]> {
      return this.http.get<Patient[]>(this.baseUrl);
    }
    getPatient(id): Observable<Patient> {
      console.log('da');
      return this.http.get<Patient>(this.baseUrl + id);
    }

    updatePatient(patient: Patient) {
      return this.http.put(this.baseUrl, patient);
    }

    deletePatient(patient: Patient) {
      return this.http.put(this.baseUrl + '/delete', patient);
    }
}
