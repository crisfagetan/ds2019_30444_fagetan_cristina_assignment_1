/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MedPlanService } from './medPlan.service';

describe('Service: MedPlan', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MedPlanService]
    });
  });

  it('should ...', inject([MedPlanService], (service: MedPlanService) => {
    expect(service).toBeTruthy();
  }));
});
