import { Component, OnInit } from '@angular/core';
import { Patient } from '../_models/patient';
import { PatientService } from '../_services/patient.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CaregiverService } from '../_services/caregiver.service';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {
  patients: Patient[];
  constructor(private patientService: PatientService, private caregiverService: CaregiverService,
              private alertify: AlertifyService, public router: Router, public route: ActivatedRoute) { }

  fromCaregiver = false;
  addPatient = false;

  ngOnInit() {
    console.log(this.route.url);
    if (this.router.url === '/patient') {
      this.showPatients();
      this.fromCaregiver = false;
      this.addPatient = false;
    } else if (this.router.url.includes('add')) { // === 'caregiver/patients/add/:id'
      this.showPatients();
      this.fromCaregiver = true;
      this.addPatient = true;
    } else {
      this.showPatientsForCaregiver();
      this.fromCaregiver = true;
      this.addPatient = false;
    }
  }

  addPatientForCaregiver(patient) {
    this.caregiverService.addPatientToCaregiver(patient, +this.route.snapshot.paramMap.get('id')).subscribe(next => {
      this.alertify.success('Patient added successfully');
      this.router.navigateByUrl('/caregiver');
    }, error => {
      this.alertify.error(error);
    });
  }


  showPatientsForCaregiver() {
    this.caregiverService.getPatients(+this.route.snapshot.paramMap.get('id')).subscribe((patients: Patient[]) => {
      this.patients = patients;
    }, error => {
      this.alertify.error(error);
    });
  }

  showPatients() {
    this.patientService.getPatients().subscribe((patients: Patient[]) => {
      this.patients = patients;
    }, error => {
      this.alertify.error(error);
    });
  }

  deleteRow(patient) {
    this.patientService.deletePatient(patient).subscribe(next => {
      this.alertify.success('Patient deleted successfully');
      this.ngOnInit();
    }, error => {
      this.alertify.error(error);
    });
}


}
