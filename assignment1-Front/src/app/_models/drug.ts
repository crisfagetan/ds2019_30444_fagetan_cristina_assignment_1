export interface Drug {

  id: number;
  name: string;
  sideEffects?: string;
  dosage?: number;

}
