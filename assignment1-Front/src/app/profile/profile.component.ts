import { Component, OnInit } from '@angular/core';
import { Patient } from '../_models/patient';
import { CaregiverService } from '../_services/caregiver.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientService } from '../_services/patient.service';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public patient: Patient;
  constructor(private patientService: PatientService, private caregiverService: CaregiverService,
              private alertify: AlertifyService, public router: Router, public route: ActivatedRoute) {
                // this.patients = [];
               }

  ngOnInit() {
   this.showCurrentPatient();
  //  console.log(this.patients);
  }


  showCurrentPatient() {
    this.patientService.getPatient(+localStorage.getItem('userRoleId')).subscribe((patient: Patient) => {
      console.log(patient);
      this.patient = patient;
      console.log(this.patient);
    }, error => {
      this.alertify.error(error);
    });
  }

  getId(): string {
    return localStorage.getItem('userRoleId');
  }





}
