import { Component, OnInit } from '@angular/core';
import { Caregiver } from '../_models/caregiver';
import { CaregiverService } from '../_services/caregiver.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {
caregivers: Caregiver[];
  constructor(private caregiverService: CaregiverService, private alertify: AlertifyService, public router: Router) { }


  ngOnInit() {
    this.showCaregivers();
  }

  showCaregivers() {
    this.caregiverService.getCaregivers().subscribe((caregivers: Caregiver[]) => {
      this.caregivers = caregivers;
    }, error => {
      this.alertify.error(error);
    });
  }

  deleteRow(caregiver) {
    this.caregiverService.deleteCaregiver(caregiver).subscribe(next => {
      this.alertify.success('Caregiver deleted successfully');
      this.showCaregivers();
    }, error => {
      this.alertify.error(error);
    });
}


}
