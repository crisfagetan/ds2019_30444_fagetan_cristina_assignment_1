import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  out = false;

  constructor(private authService: AuthService, public router: Router) { }

  ngOnInit() {
  }

  redirectPatients() {
    if (this.getRole() === 1) { // if caregiver
      this.router.navigateByUrl('/caregiver/patients/' + this.getUserInRoleId());
    } else {
      this.router.navigateByUrl('/patient');
    }
  }

  getRole(): number {
    return +localStorage.getItem('userRole');
  }

  getId(): number {
    return +localStorage.getItem('userId');
  }

  getUserInRoleId(): number {
    return +localStorage.getItem('userRoleId');
  }

  logOut() {
    localStorage.setItem('out', '1');
  }

  getOut(): number {
    return +localStorage.getItem('out');
  }
}
