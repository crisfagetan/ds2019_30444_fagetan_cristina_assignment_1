import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { User } from '../_models/user';
import { UserRegister } from '../_models/userRegister';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public formBuilder: FormBuilder, private authService: AuthService, private alertify: AlertifyService,
              private route: ActivatedRoute, public router: Router) { }
  userRegister: UserRegister;

  registrationForm = this.formBuilder.group({
    username: new FormControl(''),
    password: new FormControl('') ,
    roleSelect: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    dateOfBirth: new FormControl(''),
    genderSelect: new FormControl(''),
    address: new FormControl('')
  });

  ngOnInit() {
  }


  register() {
    this.userRegister = new UserRegister();
    this.userRegister.username = this.registrationForm.get('username').value;
    this.userRegister.password = this.registrationForm.get('password').value;
    this.userRegister.role = +this.registrationForm.get('roleSelect').value;
    this.userRegister.firstName = this.registrationForm.get('firstName').value;
    this.userRegister.lastName = this.registrationForm.get('lastName').value;
    this.userRegister.dateOfBirth = this.registrationForm.get('dateOfBirth').value;
    this.userRegister.gender = (+this.registrationForm.get('genderSelect').value) === 1;
    this.userRegister.address = this.registrationForm.get('address').value;
    this.authService.register(this.userRegister).subscribe((user: User) => {
      localStorage.setItem('userId', JSON.stringify(user.id));
      localStorage.setItem('userRole', JSON.stringify(user.role));
      localStorage.setItem('userRoleId', JSON.stringify(user.userInRoleId));
      localStorage.setItem('out', '0');
      this.alertify.success('Logged in successfully');
    }
    , error => {
      this.alertify.error(error);
    }, () => {
      this.router.navigate(['/profile/' + localStorage.getItem('userId')]);
    });
  }


}
