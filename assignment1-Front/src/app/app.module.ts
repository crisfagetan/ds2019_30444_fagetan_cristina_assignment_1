import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DrugComponent } from './drug/drug.component';
import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { CaregiverComponent } from './caregiver/caregiver.component';
import { PatientComponent } from './patient/patient.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { DrugEditComponent } from './drug-edit/drug-edit.component';
import { AlertifyService } from './_services/alertify.service';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CaregiverEditComponent } from './caregiver-edit/caregiver-edit.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';
import { MedPlanComponent } from './medPlan/medPlan.component';
import { MedplanEditComponent } from './medplan-edit/medplan-edit.component';
// import {MatSelectModule} from '@angular/material/select';

@NgModule({
   declarations: [
      AppComponent,
      DrugComponent,
      NavComponent,
      LoginComponent,
      CaregiverComponent,
      PatientComponent,
      ProfileComponent,
      RegisterComponent,
      DrugEditComponent,
      CaregiverEditComponent,
      PatientEditComponent,
      MedPlanComponent,
      MedplanEditComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      // MatSelectModule,
      RouterModule.forRoot(appRoutes)
   ],
   providers: [
      AlertifyService,
      PreventUnsavedChanges
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
