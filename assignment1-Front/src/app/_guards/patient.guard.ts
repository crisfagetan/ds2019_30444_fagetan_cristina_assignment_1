import { Injectable } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PatientGuard implements CanActivate {

  constructor(
    private router: Router,
    private alertify: AlertifyService
  ) {}

  // No Patient has access
  canActivate(): boolean {
    if (
      localStorage.getItem('userRole') === '2' &&
      localStorage.getItem('out') === '0'
    ) {
      this.alertify.error('You shall not pass!!!');
      this.router.navigate(['/profile']);
      return false;
    }
    return true;
  }
}
