package ds.project.assignment1.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class User {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

//    @Column(name = "passwordSalt", nullable = false)
//    private byte[] passwordSalt;

    @Column(name = "role")
    private Integer role;  // 0 = doctor; 1 = caregiver; 2 = patient

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    @Column(name = "dateOfBirth")
    private Date dateOfBirth;

    @Column(name = "gender")
    private Boolean gender; //1 = male; 0 = female

    @Column(name = "address")
    private String address;

    @Column(name = "is_removed")
    private Boolean isRemoved;

    //------------------------------------------------- CONSTRUCTORS ----------------------------------------------------------------------


    public User(int id, String username, String password, int role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.isRemoved = false;
    }



    public User(String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.isRemoved = false;
    }

    public User(int  id, String username,int role) {
        this.id = id;
        this.username = username;
        this.role = role;
        this.isRemoved = false;
    }

    public User(Integer id, String firstName, String lastName, Date dateOfBirth, Boolean gender, String address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.address = address;
        this.isRemoved = false;
    }

    //------------------------------------------------- GET & SET ----------------------------------------------------------------------

//    public Boolean isFemale() {
//        return !gender;
//    }
//
//    //this is actually getGender
//    public Boolean isMale() {
//        return gender;
//    }
//
//    public Boolean isDoctor() {
//        return (role == 0);
//    }
//
//    public Boolean isCaregiver() {
//        return (role == 1);
//    }
//
//    public Boolean isPatient() {
//        return (role == 2);
//    }

}
