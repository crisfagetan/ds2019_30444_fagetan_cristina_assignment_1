package ds.project.assignment1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "doctors")
@Data
@NoArgsConstructor
@AllArgsConstructor
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Doctor {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @JsonIgnore
    @JoinColumn(name = "user_id")
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private User doctorUser;

    @JsonIgnore
    @OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MedicationPlan> medPlans;

    @Column(name = "is_removed")
    private Boolean isRemoved;

    //------------------------------------------------- CONSTRUCTORS ----------------------------------------------------------------------

    public Doctor(Integer id, List<MedicationPlan> medPlans) {
        this.id = id;
        this.medPlans = medPlans;
        this.isRemoved = false;
    }

    public Doctor(User user){
        this.doctorUser = user;
        this.id = 0;
        this.medPlans = new ArrayList<MedicationPlan>();
        this.isRemoved = false;
    }


    //------------------------------------------------- GET & SET ----------------------------------------------------------------------




}
