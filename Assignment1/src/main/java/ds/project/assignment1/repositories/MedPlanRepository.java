package ds.project.assignment1.repositories;

import ds.project.assignment1.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MedPlanRepository extends JpaRepository<MedicationPlan, Integer> {

    public List<MedicationPlan> findAllByIsRemoved(Boolean isRemoved);
    public MedicationPlan findMedicationPlanByIdAndIsRemoved(int id, Boolean isRemoved);
}
