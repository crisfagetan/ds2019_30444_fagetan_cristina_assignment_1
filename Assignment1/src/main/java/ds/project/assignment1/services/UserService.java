package ds.project.assignment1.services;


import ds.project.assignment1.dtos.UserForRegisterDTO;
import ds.project.assignment1.dtos.UserForViewDTO;
import ds.project.assignment1.entities.Caregiver;
import ds.project.assignment1.entities.Doctor;
import ds.project.assignment1.entities.Patient;
import ds.project.assignment1.entities.User;
import ds.project.assignment1.errorhandlers.DuplicateEntryException;
import ds.project.assignment1.errorhandlers.ResourceNotFoundException;
import ds.project.assignment1.repositories.CaregiverRepository;
import ds.project.assignment1.repositories.DoctorRepository;
import ds.project.assignment1.repositories.PatientRepository;
import ds.project.assignment1.repositories.UserRepository;
import ds.project.assignment1.validators.UserFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final DoctorRepository doctorRepository;
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public UserService(UserRepository userRepository, DoctorRepository doctorRepository, PatientRepository patientRepository, CaregiverRepository caregiverRepository) {
        this.userRepository = userRepository;
        this.doctorRepository = doctorRepository;
        this.caregiverRepository = caregiverRepository;
        this.patientRepository = patientRepository;
    }

    //------------------------------------------ FIND -------------------------------------------------------------------

    public List<UserForViewDTO> findAll(){
        List<User> users = userRepository.findAllByIsRemoved(false);

        return users.stream()
                .map(UserForViewDTO::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public UserForViewDTO findUserById(Integer id){
        User user  = userRepository.findUserByIdAndIsRemoved(id, false);

        if (user == null) {
            throw new ResourceNotFoundException("User", "user id", id);
        }

        UserForViewDTO userForViewDTO = UserForViewDTO.generateDTOFromEntity(user);

        switch(user.getRole()){
            //Doctor
            case 0:
                userForViewDTO.setUserInRoleId(doctorRepository.findDoctorByDoctorUser(user).getId());
                break;
            //Caregiver
            case 1:
                userForViewDTO.setUserInRoleId(caregiverRepository.findCaregiverByCaregiverUser(user).getId());
                break;
            //Patient
            case 2:
                userForViewDTO.setUserInRoleId(patientRepository.findPatientByPatientUser(user).getId());
                break;

        }
        return userForViewDTO;
    }

    //------------------------------------------------- LOGIN ---------------------------------------------------------


    public UserForViewDTO login(String username, String password){

        User user = userRepository.findUserByUsernameAndPassword(username, password);

        if (user== null) {
            throw new ResourceNotFoundException("User", "password", password);
        }

        UserForViewDTO userForViewDTO = UserForViewDTO.generateDTOFromEntity(user);

        switch(user.getRole()){
            //Doctor
            case 0:
                userForViewDTO.setUserInRoleId(doctorRepository.findDoctorByDoctorUser(user).getId());
                break;
                //Caregiver
            case 1:
                userForViewDTO.setUserInRoleId(caregiverRepository.findCaregiverByCaregiverUser(user).getId());
                break;
                //Patient
            case 2:
                userForViewDTO.setUserInRoleId(patientRepository.findPatientByPatientUser(user).getId());
                break;

        }
        return userForViewDTO;
    }



    //------------------------------------------------- REGISTER ----------------------------------------------------------

    public Integer insert(UserForRegisterDTO userForRegisterDTO) {

        UserFieldValidator.validateRegister(userForRegisterDTO);

        User user = userRepository.findByUsername(userForRegisterDTO.getUsername());
        if(user != null){
            throw  new DuplicateEntryException("User", "username", userForRegisterDTO.getUsername());
        }

        User userToInsert = UserForRegisterDTO.generateEntityFromDTO(userForRegisterDTO);


        switch(userForRegisterDTO.getRole()){
            //Insert Doctor
            case 0: DoctorService doctorService = new DoctorService(doctorRepository);
                Doctor doctor =  new Doctor(userToInsert);
                int id = doctorService.insertDoctor(doctor);
                return doctorRepository.findDoctorByIdAndIsRemoved(id, false).getDoctorUser().getId();
            //Insert Caregiver
            case 1: CaregiverService caregiverService = new CaregiverService(caregiverRepository);
                Caregiver caregiver = new Caregiver(userToInsert);
                int idC = caregiverService.insertCaregiver(caregiver);
                return caregiverRepository.findCaregiverByIdAndIsRemoved(idC, false).getCaregiverUser().getId();
            //Insert Patient
            case 2: PatientService patientService = new PatientService(patientRepository, caregiverRepository);
                Patient patient = new Patient(userToInsert);
                int idP = patientService.insertPatient(patient);
                return patientRepository.findPatientByIdAndIsRemoved(idP, false).getPatientUser().getId();


            default: return -1;
        }
    }

    //------------------------------------------------- UPDATE ---------------------------------------------------------

    public Integer update(UserForRegisterDTO userDTO) {

        User user = userRepository.findUserByIdAndIsRemoved(userDTO.getId(), false);

        if(user == null){
            throw new ResourceNotFoundException("User", "user id", userDTO.getId().toString());
        }
        UserFieldValidator.validateRegister(userDTO);

        return userRepository.save(UserForRegisterDTO.generateEntityFromDTO(userDTO)).getId();
    }

    //------------------------------------------------ DELETE ----------------------------------------------------------

    public void delete(UserForViewDTO userDTO){
        User user = userRepository.findByUsername(userDTO.getUsername());
        user.setIsRemoved(true);
        this.userRepository.save(user);
    }




}
