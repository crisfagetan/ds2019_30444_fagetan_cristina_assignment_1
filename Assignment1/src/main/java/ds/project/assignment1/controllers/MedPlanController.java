package ds.project.assignment1.controllers;

import ds.project.assignment1.dtos.CaregiverViewDTO;
import ds.project.assignment1.dtos.MedPlanViewDTO;
import ds.project.assignment1.services.CaregiverService;
import ds.project.assignment1.services.MedPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medplan")
public class MedPlanController {

    private final MedPlanService medPlanService;

    @Autowired
    public MedPlanController(MedPlanService medPlanService) {
        this.medPlanService = medPlanService;
    }

    @GetMapping(value = "/{id}")
    public MedPlanViewDTO findById(@PathVariable("id") Integer id){
        return medPlanService.findUserById(id);
    }


    @GetMapping()
    public List<MedPlanViewDTO> findAll(){
        return medPlanService.findAll();
    }

    @PutMapping()
    public Integer updateUser(@RequestBody MedPlanViewDTO medPlanViewDTO) { return medPlanService.update(medPlanViewDTO); }

    @PutMapping(value = "/delete")
    public void delete(@RequestBody MedPlanViewDTO medPlanViewDTO){
        medPlanService.delete(medPlanViewDTO);
    }

}
