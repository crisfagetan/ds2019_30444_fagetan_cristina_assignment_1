package ds.project.assignment1.controllers;


import ds.project.assignment1.dtos.DrugDTO;
import ds.project.assignment1.entities.Drug;
import ds.project.assignment1.services.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/drug")
public class DrugController {

    private final DrugService drugService;

    @Autowired
    public DrugController(DrugService  drugService) {
        this.drugService = drugService;
    }

    @GetMapping(value = "/{id}")
    public Drug findById(@PathVariable("id") Integer id){
        return drugService.findDrugById2(id);
    }

    @GetMapping()
    public List<DrugDTO> findAll(){
        return drugService.findAll();
    }

    @PostMapping()
    public Integer insertDrugDTO(@RequestBody DrugDTO drugDTO){
        return drugService.insert(drugDTO);
    }

    @PutMapping()
    public Integer updateDrug(@RequestBody DrugDTO drugDTO) {
        return drugService.update(drugDTO);
    }

    @PutMapping(value = "/delete")
    public void delete(@RequestBody DrugDTO drugDTO){
        drugService.delete(drugDTO);
    }

}
