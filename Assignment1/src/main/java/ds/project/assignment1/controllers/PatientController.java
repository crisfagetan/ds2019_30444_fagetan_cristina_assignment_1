package ds.project.assignment1.controllers;

import ds.project.assignment1.dtos.CaregiverViewDTO;
import ds.project.assignment1.dtos.MedPlanViewDTO;
import ds.project.assignment1.dtos.PatientViewDTO;
import ds.project.assignment1.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService  patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public PatientViewDTO findById(@PathVariable("id") Integer id){
        return patientService.findPatientById(id);
    }

    @GetMapping(value = "/{patientId}/medPlans")
    public List<MedPlanViewDTO> seeMedPlans(@PathVariable("patientId") Integer id){
        return patientService.getMedplans(id);
    }


    @GetMapping()
    public List<PatientViewDTO> findAll(){
        return patientService.findAll();
    }


    @PutMapping()
    public Integer updateUser(@RequestBody PatientViewDTO patientViewDTO) {
        return patientService.update(patientViewDTO);
    }

    @PutMapping(value = "/{patientId}/addCaregiver/{caregiverId}")
    public void setCaregiver(@PathVariable("patientId") Integer patientId, @PathVariable("caregiverId") Integer caregiverId) {
        patientService.addCaregiver(patientId, caregiverId);
    }

    @PutMapping(value = "/delete")
    public void delete(@RequestBody PatientViewDTO patientViewDTO){
        patientService.delete(patientViewDTO);
    }

}
